#include <iostream>
#include <sstream>
#include <iomanip>

int getchoice() { //Get function to run
	printf("What do you want to do?\n1. rgb2hex\n2. hex2rgb\n3. exit\n\n:");
	
	std::string choicestring;
	std::cin >> choicestring;

	int choice = 0;
		
	if (choicestring == "1" or choicestring == "1." or choicestring == "r2h" or choicestring == "rgb" or choicestring == "one" or choicestring == "rgb2hex"){
		choice = 1;
	} else if (choicestring == "2" or choicestring == "2." or choicestring == "h2r" or choicestring == "hex" or choicestring == "two" or choicestring == "hex2rgb"){
		choice = 2;
	} else if (choicestring == "3" or choicestring == "3." or choicestring == "x" or choicestring == "e" or choicestring == "q" or choicestring == "exit"){
		choice = 3;
	}
	return choice;
}

std::string getinput() { //Get color input
	printf("What is your color?\n\n:");
	
	std::string input;
	std::cin >> input;
	
	return input;
}	

std::string rgb2hex(std::string rgb) { //RGB to HEX conversion
	std::string answer = "";
	std::string arr[3];
	std::stringstream ss;
	std::istringstream iss(rgb);
	std::string token;
	int i = 0;
	int num = 0;

	while (std::getline(iss, token, ',')) {
		arr[i] += token;
		++i;
	}

	for(i = 0; i < 3; ++i) {
		num = std::stoi(arr[i]);
		if (std::stoi(arr[i]) <= 255){
		ss << std::setfill('0') << std::setw(2) << std::hex << num;
		}
		else {
			ss << "??";
		}
	}

	answer = "#" + ss.str() + "\n";	
	return answer;
}

std::string hex2rgb(std::string hexa) { //HEX to RGB conversion
	std::string answer = "";
	int counter = 0;
	int modulecounter = 0;
	int countnum = 2;
	int len = hexa.size();
	int num = 0;
	std::string curnumb = "";
	if (hexa[0] == '#') {
		hexa.erase(0,1);
	}
	if (len == 3){
		countnum = 1;
	}

	for (char& character : hexa) {
		if (counter != countnum and modulecounter != 3) {
			curnumb += character;
			counter += 1;
		}
		if (counter == countnum) {
			if (len == 3){
				num = std::stoi((curnumb + curnumb), 0, 16);
			}
			else {
				num = std::stoi(curnumb, 0, 16);
			}
			if (modulecounter != 2) {
				answer += std::to_string(num) + ",";
			}
			else {
				answer += std::to_string(num) + "\n";
			}
			curnumb = "";
			counter = 0;
			modulecounter += 1;
		}
	}
	return answer;
}

int main() {
	std::string answer = "Unknown failure";	//initialize final answer string
	
	int choice = getchoice(); //get if user wants hex or rgb conversion

	if (choice == 3){ //exit program
		std::cout << "Exited program." << std::endl;
		return 0;
	}
	else if (choice == 0){ //invalid option
		std::cout << "Invalid option. Exiting program." << std::endl;
		return 1;
	}

	std::string input = getinput(); //get color value
	
	if (choice == 1){
		answer = rgb2hex(input);
	}
	else if (choice == 2){
		answer = hex2rgb(input);
	}

	std::cout << answer;

	return 0; //end program
}
